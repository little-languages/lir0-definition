import           Data.ByteString.Internal (packChars, unpackChars)
import qualified Data.ByteString.UTF8 as BLU
import qualified Data.HashMap.Strict  as Map
import qualified Data.List            as List
import qualified Data.Text            as Text
import qualified Data.Vector          as Vector
import qualified Data.Yaml            as Yaml
import qualified Text.PrettyPrint

import           Test.Hspec

import           LIR0.Lexical           (Token(..), lexeme, scanner, token)
import           LIR0.Location          (Locationable(..))

import qualified LIR0.Dynamic
import qualified LIR0.Dynamic.AST
import qualified LIR0.GMachine.M1
import qualified LIR0.GMachine.M1.Compiler
import qualified LIR0.GMachine.M2
import qualified LIR0.GMachine.M2.Compiler
import qualified LIR0.GMachine.M3
import qualified LIR0.GMachine.M3.Compiler
import qualified LIR0.GMachine.M4
import qualified LIR0.GMachine.M4.Compiler
import qualified LIR0.GMachine.M5
import qualified LIR0.GMachine.M5.Compiler
import qualified LIR0.GMachine.M6
import qualified LIR0.GMachine.M6.Compiler
import qualified LIR0.Static
import           LIR0.PrettyPrint

data Tests
  = Describe Text.Text [Tests]
  | Test Text.Text Text.Text Yaml.Value

loadFile :: String -> IO Text.Text
loadFile name = do
  content <- readFile name
  return $ Text.pack content

main :: IO ()
main = do
  lexicalContent <- loadFile "./lexical.yaml"
  parserContent <- loadFile "./parser.yaml"
  dynamicContent <- loadFile "./dynamic.yaml"
  m1Content <- loadFile "./m1.yaml"
  m2Content <- loadFile "./m2.yaml"
  m3Content <- loadFile "./m3.yaml"
  m4Content <- loadFile "./m4.yaml"
  m5Content <- loadFile "./m5.yaml"
  m6Content <- loadFile "./m6.yaml"
--  _ <- dumpParserContent parserContent
--  _ <- dumpDynamicContent dynamicContent
  hspec $
    describe "P0" $
    describe "Conformance Tests" $ do
      doTest lexicalAssert $ loadTests "Lexer" lexicalContent
      doTest parserAssert $ loadTests "Parser" parserContent
      doTest dynamicAssert $ loadTests "Dynamic" dynamicContent
      doTest m1Assert $ loadTests "G-Machine M1" m1Content
      doTest m2Assert $ loadTests "G-Machine M2" m2Content
      doTest m3Assert $ loadTests "G-Machine M3" m3Content
      doTest m4Assert $ loadTests "G-Machine M4" m4Content
      doTest m5Assert $ loadTests "G-Machine M5" m5Content
      doTest m6Assert $ loadTests "G-Machine M6" m6Content
  where
    lexicalAssert :: Text.Text -> Yaml.Value -> Expectation
    lexicalAssert input output =
      case output of
        Yaml.Array output' -> input' `shouldBe` vectorToTexts output'
        _                  -> input' `shouldBe` []
      where
        input' = map Text.pack $ map (\s -> show (token s) ++ " " ++ show (location s) ++ " [" ++ lexeme s ++ "]") $ List.filter (\s -> token s /= WhiteSpace) $ scanner $ Text.unpack input
    parserAssert :: Text.Text -> Yaml.Value -> Expectation
    parserAssert input output =
      (pp . either toYaml toYaml . LIR0.Static.parse . Text.unpack) input `shouldBe` pp output
    dynamicAssert input output =
      (either (pp . toYaml) (packChars . Text.PrettyPrint.render . LIR0.Dynamic.AST.ppProgram) . LIR0.Dynamic.parse . Text.unpack) input `shouldBe` (packChars . Text.unpack . Text.stripEnd . valueToText) output
    m1Assert input output =
      let gmstates =
            either (\_ -> []) LIR0.GMachine.M1.eval $ LIR0.GMachine.M1.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = Text.PrettyPrint.render $ LIR0.GMachine.M1.showStackItemValue gmstate (List.head $ LIR0.GMachine.M1.getStack gmstate)
                       returnStats = LIR0.GMachine.M1.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output
    m2Assert input output =
      let gmstates =
            either (\_ -> []) LIR0.GMachine.M2.eval $ LIR0.GMachine.M2.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = Text.PrettyPrint.render $ LIR0.GMachine.M2.showStackItemValue gmstate (List.head $ LIR0.GMachine.M2.getStack gmstate)
                       returnStats = LIR0.GMachine.M2.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output
    m3Assert input output =
      let gmstates =
            either (\z -> error (show z)) LIR0.GMachine.M3.eval $ LIR0.GMachine.M3.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = Text.PrettyPrint.render $ LIR0.GMachine.M3.showStackItemValue gmstate (List.head $ LIR0.GMachine.M3.getStack gmstate)
                       returnStats = LIR0.GMachine.M3.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output
    m4Assert input output =
      let gmstates =
            either (\z -> error (show z)) LIR0.GMachine.M4.eval $ LIR0.GMachine.M4.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = Text.PrettyPrint.render $ LIR0.GMachine.M4.showStackItemValue gmstate (List.head $ LIR0.GMachine.M4.getStack gmstate)
                       returnStats = LIR0.GMachine.M4.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output
    m5Assert input output =
      let gmstates =
            either (\z -> error (show z)) LIR0.GMachine.M5.eval $ LIR0.GMachine.M5.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = Text.PrettyPrint.render $ LIR0.GMachine.M5.showStackItemValue gmstate (List.head $ LIR0.GMachine.M5.getStack gmstate)
                       returnStats = LIR0.GMachine.M5.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output
    m6Assert input output =
      let gmstates =
            either (\z -> error (show z)) LIR0.GMachine.M6.eval $ LIR0.GMachine.M6.Compiler.parse $ Text.unpack input
          (result, steps) =
            case gmstates of
              [] -> ("0", 0)
              _ -> let gmstate = List.last gmstates
                       returnValue = LIR0.GMachine.M6.getOutput gmstate
                       returnStats = LIR0.GMachine.M6.getStats gmstate
                    in (returnValue, returnStats)
      in fromMap [("result", fromString result), ("steps", fromInt steps)] `shouldBe` output

    doTest assert test =
      let doTests' [] = return ()
          doTests' (x:xs) = do
            doTest' x
            doTests' xs

          doTest' (Describe name tests) = describe (Text.unpack name) $ doTests' tests
          doTest' (Test name input output) = it (Text.unpack name) $ assert input output
       in doTest' test

dumpParserContent :: Text.Text -> IO ()
dumpParserContent content = do
  let tests = loadTests "Parser" content
  runTests tests
    where runTests (Describe _ tests) =
            mapM_ runTests tests
          runTests (Test name input _) = do
            putStrLn $ Text.unpack $ name
            putStrLn $ unpackChars . pp . either toYaml toYaml . LIR0.Static.parse . Text.unpack $ input

dumpDynamicContent :: Text.Text -> IO ()
dumpDynamicContent content = do
  let tests = loadTests "Dynamic" content
  runTests tests
    where runTests (Describe _ tests) =
            mapM_ runTests tests
          runTests (Test name input _) = do
            putStrLn $ Text.unpack $ name
            putStrLn $ unpackChars . (either (pp . toYaml) (packChars . Text.PrettyPrint.render . LIR0.Dynamic.AST.ppProgram) . LIR0.Dynamic.parse . Text.unpack) $ input

loadTests :: Text.Text -> Text.Text -> Tests
loadTests name content =
  case Yaml.decodeEither' $ BLU.fromString $ Text.unpack content :: Either Yaml.ParseException Yaml.Value of
    Left error -> Describe name [Test "Load file" (Text.pack $ show error) (fromString "")]
    Right (Yaml.Array a) -> Describe name (loadTestVector a)
    Right _ -> Describe name []

loadTestVector :: Vector.Vector Yaml.Value -> [Tests]
loadTestVector v =
  if Vector.null v
    then []
    else let hd = Vector.head v
             rest = Vector.tail v
             o =
               case hd of
                 Yaml.Object o' -> o'
                 _              -> Map.empty
          in if Map.member "scenario" o
               then case Map.lookupDefault Yaml.Null "scenario" o of
                      Yaml.Object m ->
                        let name = Map.lookupDefault Yaml.Null "name" m
                            tests = Map.lookupDefault Yaml.Null "tests" m
                            testResults =
                              case tests of
                                Yaml.Array tests' -> loadTestVector tests'
                                _                 -> []
                         in Describe (valueToText name) testResults : loadTestVector rest
                      _ -> loadTestVector rest
               else let name = valueToText $ Map.lookupDefault Yaml.Null "name" o
                        input = valueToText $ Map.lookupDefault Yaml.Null "input" o
                        output = Map.lookupDefault Yaml.Null "output" o
                     in Test name input output : loadTestVector rest

vectorToTexts :: Vector.Vector Yaml.Value -> [Text.Text]
vectorToTexts v = Vector.toList $ Vector.map (Text.strip . valueToText) v
