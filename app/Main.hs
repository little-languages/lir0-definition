module Main where

import           Data.List
import           System.Environment        (getArgs)
import qualified Text.PrettyPrint          as PP

import qualified LIR0.GMachine.M6
import qualified LIR0.GMachine.M6.Compiler

names :: String -> (String, String)
names n =
  let n' =
        if ".lir0" `isSuffixOf` n
          then n
          else n <> ".lir0"
      baseN = take (length n' - 5) n'
   in (n', baseN)

run :: String -> IO ()
run name = do
  let (name', _) = names name
  content <- readFile name'
  case LIR0.GMachine.M6.Compiler.parse content of
    Left e -> putStrLn $ "Error: " <> show e
    Right c -> putStrLn $ PP.render $ LIR0.GMachine.M6.showResults $ LIR0.GMachine.M6.eval c

showErrorOptions :: String -> IO ()
showErrorOptions error = do
  putStrLn $ "Error: " <> error
  showOptions

showOptions :: IO ()
showOptions = do
  putStrLn "Options: run file"
  putStrLn "  --help"
  putStrLn "    Show all help options."
  putStrLn ""
  putStrLn "  run file"
  putStrLn "    Interpret the contents of file[.lir0]."

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> showErrorOptions "No arguments supplied"
    ["--help"] -> showOptions
    ["run", n] -> run n
    _ -> showErrorOptions ("Invalid arguments: " <> unwords args)