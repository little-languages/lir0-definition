# LIR0 - Definition

LIR0 is a faithful representation of the intermediate language defined in the tutorial book [Implementing functional languages: a tutorial](https://www.microsoft.com/en-us/research/publication/implementing-functional-languages-a-tutorial/) by Simon Peyton Jones and David Lester; reference to this tutorial will refer to *The Book*.

This definition or reference implementation includes the following features of the book's `Core Language`:

- Lexical analyses
- Static Syntax Analysis
- Dynamic Syntax Analysis
- Definition of a minimal G-machine
- Translation into a minimal G-machine
- Interpreter for a minimal G-machine

As this code forms part of the [littlelanguages](https://little-languages.gitlab.io/overview) family, the implementation of `LIRO` and the G-machine follows the [littlelanguages](https://little-languages.gitlab.io/overview) style of [definition](https://little-languages.gitlab.io/overview/lir0/overview.html).

## Building the Source

This implementation is interpreted so all libraries are managed using [Haskell Took Stack](https://docs.haskellstack.org/en/stable/README/). With *stack* installed and this project checked out then issuing

```bash
stack test
```

will download all dependencies and run the suite of tests.  The tests are comprehensive and drive off of a collection of YAML files for each compilation phase.  Each file includes a number of different scenarios that collectively make up the acceptance tests:

* [lexical.yaml](https://gitlab.com/little-languages/lir0-definition/-/raw/master/lexical.yaml)
* [parser.yaml](https://gitlab.com/little-languages/lir0-definition/-/raw/master/parser.yaml)
* [dynamic.yaml](https://gitlab.com/little-languages/lir0-definition/-/raw/master/dynamic.yaml)
* [m1.yaml](https://gitlab.com/little-languages/lir0-definition/-/raw/master/m1.yaml)

Issuing

```bash
./scripts/mk-build
```

Will build the binary `lir0-haskell-exe` and place it into `./build/bin`.  Using this binary samples can then can be run.  For example

```bash
Graemes-iMac:lir0-definition graemelockley$ ./build/bin/lir0-haskell-exe run samples/s0.lir0 
Supercombinator Definitions
  I: #1
    Push 0
    Slide 2
    Unwind
  K: #2
    Push 0
    Slide 3
    Unwind
  K1: #3
    Push 1
    Slide 3
    Unwind
  S: #4
    Push 2
    Push 2
    MkAp
    Push 3
    Push 2
    MkAp
    MkAp
    Slide 4
    Unwind
  compose: #5
    Push 2
    Push 2
    MkAp
    Push 1
    MkAp
    Slide 4
    Unwind
  main: #7
    PushInt 3
    PushGlobal I
    MkAp
    Slide 1
    Unwind
  twice: #6
    Push 0
    Push 1
    PushGlobal compose
    MkAp
    MkAp
    Slide 2
    Unwind
State Transitions
  . PushGlobal main
    Unwind
  . - #7: Global main
    Unwind
  . - #7: Global main
    PushInt 3
    PushGlobal I
    MkAp
    Slide 1
    Unwind
  . - #7: Global main
    - #8: 3
    PushGlobal I
    MkAp
    Slide 1
    Unwind
  . - #7: Global main
    - #8: 3
    - #1: Global I
    MkAp
    Slide 1
    Unwind
  . - #7: Global main
    - #9: Ap #1 #8
    Slide 1
    Unwind
  . - #9: Ap #1 #8
    Unwind
  . - #9: Ap #1 #8
    - #1: Global I
    Unwind
  . - #9: Ap #1 #8
    - #1: Global I
    Push 0
    Slide 2
    Unwind
  . - #9: Ap #1 #8
    - #1: Global I
    - #8: 3
    Slide 2
    Unwind
  . - #8: 3
    Unwind
  . - #8: 3
Steps Taken
  11
Graemes-iMac:lir0-definition graemelockley$ 
```

## Orientation to *The Book*

The definition of the G-Machine and the various iterations have been kept as similar to the book as I could.  I resisted transforming the code to use monads or anything like that but kept the code as true as I could to the original Miranda code.

The following set of points describe unpack the similarities and differences from *The Book* in a little more detail.

* The syntax of the `core language` has been kept as is.  No effort has been made in `LIR0` to expand the language to include more data types, built in libraries or to support even a rudimentary module system.
* The [parser](https://gitlab.com/little-languages/lir0-definition/-/raw/master/src/LIR0/Static.hs) has been built using [Parsec](https://hackage.haskell.org/package/parsec) rather than hand cranking the parsing combinators from scratch.
* The parser produces an [abstract tree](https://gitlab.com/little-languages/lir0-definition/-/raw/master/src/LIR0/Static/AST.hs) which includes the [location](https://gitlab.com/little-languages/lir0-definition/-/blob/master/src/LIR0/Location.hs) and then that is [translated](https://gitlab.com/little-languages/lir0-definition/-/raw/master/src/LIR0/Dynamic.hs) into the same [AST](https://gitlab.com/little-languages/lir0-definition/-/raw/master/src/LIR0/Dynamic/AST.hs) that is used in *The Book*.  The reason for having the intermediate representation is that it is easier to confirm that referenced supercombinators and parameter names are valid.  Initial attempts to write this software rain afoul with trivial errors in the source text.
* The pretty printers uses [pretty](https://hackage.haskell.org/package/pretty), a Haskell pretty-printing library, rather than hand cranking.
* The G-Machine is broken out into a separate implementation for each of the different Mark refinements.       
