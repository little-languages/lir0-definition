- name: Constant
  input: |
    main = 3
  output:
    result: '3'
    steps: 9
- name: Identity constant
  input: |
    main = I 3
  output:
    result: '3'
    steps: 21
- name: Composed identity constant
  input: |
    id = S K K ;
    main = id 3
  output:
    result: '3'
    steps: 51
- name: Duplicate reduction of common expression
  input: |
    main = twice (I I I) 3
  output:
    result: '3'
    steps: 80
- name: Infinite list
  input: |
    cons a b cc cn = cc a b ;
    nil cc cn = cn ;
    hd list = list K abort ;
    tl list = list K1 abort ;
    abort = abort ;

    many x = cons x (many x) ;

    main = hd (tl (tl (tl (tl (tl (tl (many 10)))))))
  output:
    result: '10'
    steps: 427
- name: Duplicate reductions
  input: |
    twice2 f x = f (f x) ;
    id x = x ;
    main = twice2 twice2 id 3
  output:
    result: '3'
    steps: 102
- name: Let expression
  input: |
    main = let id1 = I I I
            in id1 id1 3
  output:
    result: '3'
    steps: 54
- name: Nested let expression
  input: |
    oct g x = let h = twice g in
              let k = twice h
               in k (k x) ;
    main = oct I 4
  output:
    result: '4'
    steps: 216
- name: Letrec expression
  input: |
    main = letrec id1 = I I I
               in id1 id1 3
  output:
    result: '3'
    steps: 58
- name: Nested letrec expression
  input: |
    oct g x = letrec h = twice g in
              letrec k = twice h
                  in k (k x) ;
    main = oct I 4
  output:
    result: '4'
    steps: 226
- name: Infinite list using letrec
  input: |
    cons a b cc cn = cc a b ;
    nil cc cn = cn ;
    hd list = list K abort ;
    tl list = list K1 abort ;
    abort = abort ;

    many x = letrec xs = cons x xs
                 in xs ;

    main = hd (tl (tl (tl (tl (tl (tl (many 10)))))))
  output:
    result: '10'
    steps: 340
- scenario:
    name: Arithmatic Operators
    tests:
      - name: '+'
        input: |
          main = 3+4
        output:
          result: '7'
          steps: 11
      - name: '-'
        input: |
          main = 3-4
        output:
          result: '-1'
          steps: 11
      - name: '*'
        input: |
          main = 3*4
        output:
          result: '12'
          steps: 11
      - name: '/'
        input: |
          main = 13/3
        output:
          result: '4'
          steps: 11
      - name: 'negate'
        input: |
          main = negate 13
        output:
          result: '-13'
          steps: 10
- scenario:
    name: Comparison Operators
    tests:
      - scenario:
          name: '=='
          tests:
            - name: 'true'
              input: |
                main = 3==3
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 3==4
              output:
                result: '0'
                steps: 11
      - scenario:
          name: '~='
          tests:
            - name: 'true'
              input: |
                main = 3~=4
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 3~=3
              output:
                result: '0'
                steps: 11
      - scenario:
          name: '<'
          tests:
            - name: 'true'
              input: |
                main = 3 < 4
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 3 < 3
              output:
                result: '0'
                steps: 11
      - scenario:
          name: '<='
          tests:
            - name: 'true'
              input: |
                main = 3 <= 3
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 4 < 3
              output:
                result: '0'
                steps: 11
      - scenario:
          name: '>'
          tests:
            - name: 'true'
              input: |
                main = 4 > 3
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 3 > 3
              output:
                result: '0'
                steps: 11
      - scenario:
          name: '>='
          tests:
            - name: 'true'
              input: |
                main = 3 >= 3
              output:
                result: '1'
                steps: 11
            - name: 'false'
              input: |
                main = 3 >= 4
              output:
                result: '0'
                steps: 11
      - name: Nested arithmetic
        input: |
          main = 4*5+(2-5)
        output:
          result: '17'
          steps: 15
      - name: Higher order arithmetic
        input: |
          plus1 x = x + 1 ;
          main = twice twice plus1 4
        output:
          result: '8'
          steps: 158
      - name: List length
        input: |
          cons a b cc cn = cc a b ;
          nil cc cn = cn ;
          hd list = list K abort ;
          tl list = list K1 abort ;
          abort = abort ;

          length xs = xs length1 0 ;
          length1 x xs = 1 + (length xs) ;

          main = length (cons 3 (cons 3 (cons 3 nil)))
        output:
          result: '3'
          steps: 166
- scenario:
    name: If
    tests:
      - name: fac
        input: |
          fac n = if (n == 0) 1 (n * fac (n - 1)) ;
          main = fac 5
        output:
          result: '120'
          steps: 218
      - name: gcd
        input: |
          gcd a b = if (a == b) a if (a < b) (gcd b a) (gcd b (a - b));
          main = gcd 6 10
        output:
          result: '2'
          steps: 496
- scenario:
    name: Data structures
    tests:
      - name: Constant list
        input: |
          cons x xs = Pack{2, 2} x xs ;
          nil = Pack{1, 0} ;

          main = cons 10 (cons 11 nil)
        output:
          result: |-
            10
            11
          steps: 54
      - name: downfrom
        input: |
          cons x xs = Pack{2, 2} x xs ;
          nil = Pack{1, 0} ;

          downfrom n =
            if (n == 0)
              nil
              cons n (downfrom (n - 1)) ;

          main = downfrom 4
        output:
          result: |-
            4
            3
            2
            1
          steps: 403
