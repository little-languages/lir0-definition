module LIR0.Location
  ( combine
  , mkPosition
  , Point(..)
  , Location(..)
  , Locationable(..)
  , positionLength
  , initialPoint
  , initialPosition
  , startOffset
  ) where

import LIR0.PrettyPrint
import qualified Data.Text      as Text
import qualified Data.Yaml      as Yaml

data Location
  = LocationRange Point Point
  | LocationPoint Point
  deriving (Eq)

data Point =
  Point Int
  deriving (Eq)

initialPosition :: Location
initialPosition = LocationPoint initialPoint

initialPoint :: Point
initialPoint = Point 0

startOffset :: Location -> Int
startOffset (LocationRange (Point s) _) = s
startOffset (LocationPoint (Point s)) = s

instance Show Location where
  show (LocationRange p1 p2) = show p1 ++ "-" ++ show p2
  show (LocationPoint p)     = show p

instance Show Point where
  show (Point offset) = show offset

mkPosition :: Point -> Int -> Location
mkPosition p 0 = LocationPoint p
mkPosition p 1 = LocationPoint p
mkPosition p@(Point offset) length = LocationRange p $ Point (offset + length')
  where
    length' = length - 1

combine :: Location -> Location -> Location
combine (LocationPoint p1) (LocationPoint p2) = LocationRange (min p1 p2) (max p1 p2)
combine (LocationPoint p1) (LocationRange p21 p22) = LocationRange (min p1 p21) (max p1 p22)
combine (LocationRange p11 p12) (LocationPoint p2) = LocationRange (min p11 p2) (max p12 p2)
combine (LocationRange p11 p12) (LocationRange p21 p22) = LocationRange (min p11 p21) (max p12 p22)

instance Ord Point where
  (<=) (Point o1) (Point o2) = o1 <= o2

positionLength :: Location -> Int
positionLength (LocationRange (Point oS) (Point oE)) = oE - oS
positionLength (LocationPoint _)                     = 1

class Locationable p where
  location :: p -> Location

instance Locationable p => Locationable [p] where
  location [] = initialPosition
  location (x:[]) = location x
  location (x:xs) = location x `combine` location xs

instance Yamlable Location where
  toYaml = Yaml.String . Text.pack . show
