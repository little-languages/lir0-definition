module LIR0.Errors where

import LIR0.PrettyPrint

data Error
  = SyntaxError String
  deriving (Eq, Show)


instance Yamlable Error where
  toYaml (SyntaxError s) = fromMap [("SyntaxError", fromString s)]