{-# LANGUAGE FlexibleContexts #-}

module LIR0.Static where

import           Control.Applicative
import qualified Text.Parsec         as P
import           Text.Parsec.Pos     (initialPos)

import qualified LIR0.Errors         as Errors
import           LIR0.Lexical        (Symbol, Token(..), lexeme, scanner, token)
import           LIR0.Location       (Locationable(..), combine)
import qualified LIR0.Static.AST     as AST

type Parser a
  = P.Parsec [Symbol] () a

parse :: String -> Either Errors.Error AST.Program
parse src =
  let scanner' :: String -> [Symbol]
      scanner' src =
        filter (\s -> let t = token s
                       in t /= WhiteSpace && t /= SingleLineComment && t /= MultiLineComment) $ scanner src

      symbols :: [Symbol]
      symbols = scanner' src
   in case P.parse program "" symbols of
        Left e -> Left $ Errors.SyntaxError $ show e
        Right e -> Right e

program :: Parser AST.Program
program =
  AST.Program <$> P.sepBy sc semicolonToken <* eosToken

sc :: Parser AST.SCDefn
sc =
  (\n ns e -> AST.SCDefn ((location n) `combine` (location e)) n ns e) <$> identifier <*> identifiers <* equalToken <*> expression

identifiers :: Parser [AST.Name]
identifiers =
  P.many identifier

expression :: Parser AST.Expr
expression =
  P.chainr1 andExpression (operator "|" barToken)

andExpression :: Parser AST.Expr
andExpression =
  P.chainr1 relationalExpression (operator "&" ampersandToken)

relationalExpression :: Parser AST.Expr
relationalExpression =
  let op :: AST.Expr -> Maybe (AST.Expr -> AST.Expr -> AST.Expr, AST.Expr) -> AST.Expr
      op l Nothing = l
      op l (Just (op, r)) = op l r

      suffix :: Parser (AST.Expr -> AST.Expr -> AST.Expr, AST.Expr)
      suffix = (,) <$> relationalOp <*> additiveExpression

      relationalOp :: Parser (AST.Expr -> AST.Expr -> AST.Expr)
      relationalOp =
        operator "==" equalEqualToken <|>
        operator "~=" tildeEqualToken <|>
        operator "<" lessThanToken <|>
        operator "<=" lessEqualToken <|>
        operator ">" greaterThanToken <|>
        operator ">=" greaterEqualToken
   in op <$> additiveExpression <*> P.optionMaybe suffix

additiveExpression :: Parser AST.Expr
additiveExpression =
   let additiveOp =
         operator "+" plusToken <|>
         operator "-" minusToken
    in P.chainr1 multiplicativeExpression additiveOp

multiplicativeExpression :: Parser AST.Expr
multiplicativeExpression =
   let multiplicativeOp =
         operator "*" starToken <|>
         operator "/" slashToken
    in P.chainr1 applicativeExpression multiplicativeOp

applicativeExpression :: Parser AST.Expr
applicativeExpression =
  foldl1 AST.EAp <$> P.many1 factor

operator :: String -> Parser Symbol -> Parser (AST.Expr -> AST.Expr -> AST.Expr)
operator txt token =
    (\t -> \l r -> AST.EAp (AST.EAp (AST.EVar (location t) txt) l) r) <$> token

factor :: Parser AST.Expr
factor =
  letFactor <|> letrecFactor <|> caseFactor <|> lambdaFactor <|> identifierFactor <|> literalIntFactor <|> packFactor <|> parenthesisFactor

letFactor :: Parser AST.Expr
letFactor =
  (\l ds e -> AST.ELet (location l `combine` location e) AST.NonRecursive ds e) <$> letToken <*> P.sepBy1 defn semicolonToken <* inToken <*> expression

letrecFactor :: Parser AST.Expr
letrecFactor =
  (\l ds e -> AST.ELet (location l `combine` location e) AST.Recursive ds e) <$> letrecToken <*> P.sepBy1 defn semicolonToken <* inToken <*> expression

caseFactor :: Parser AST.Expr
caseFactor =
  (\c e alters -> AST.ECase (location c `combine` location alters) e alters) <$> caseToken <*> expression <* ofToken <*> P.sepBy1 alt semicolonToken

lambdaFactor :: Parser AST.Expr
lambdaFactor =
  (\b ns e -> AST.ELam (location b `combine` location e) ns e) <$> backslashToken <*> P.many1 identifier <* periodToken <*> expression

identifierFactor :: Parser AST.Expr
identifierFactor =
  (\t -> AST.EVar (location t) (lexeme t)) <$> identifierToken

literalIntFactor :: Parser AST.Expr
literalIntFactor =
  (\t -> AST.ENum $ AST.ConstantInt (location t) (lexeme t)) <$> literalIntToken

packFactor :: Parser AST.Expr
packFactor =
  (\p t n r -> AST.EConstr (location p `combine` location r) (AST.ConstantInt (location t) (lexeme t)) (AST.ConstantInt (location n) (lexeme n)))
    <$> packToken <* lcurlyToken <*> literalIntToken <* commaToken <*> literalIntToken <*> rcurlyToken

parenthesisFactor :: Parser AST.Expr
parenthesisFactor =
  (\l e r -> AST.EParen (location l `combine` location r) e) <$> lparenToken <*> expression <*> rparenToken

defn :: Parser (AST.Name, AST.Expr)
defn =
  (,) <$> identifier <* equalToken <*> expression

alt :: Parser AST.Alter
alt =
  (\l n ns e -> AST.Alter (location l `combine` location e) n ns e) <$> lessThanToken <*> literalInt <* greaterThanToken <*> identifiers <* minusGreaterThanToken <*> expression

identifier :: Parser AST.Name
identifier =
  (\t -> AST.Name (location t) (lexeme t)) <$> identifierToken

literalInt :: Parser AST.ConstantInt
literalInt =
  (\t -> AST.ConstantInt (location t) (lexeme t)) <$> literalIntToken


ampersandToken :: Parser Symbol
ampersandToken = satisfy $ isToken Ampersand

backslashToken :: Parser Symbol
backslashToken = satisfy $ isToken Backslash

barToken :: Parser Symbol
barToken = satisfy $ isToken Bar

caseToken :: Parser Symbol
caseToken = satisfy $ isToken Case

commaToken :: Parser Symbol
commaToken = satisfy $ isToken Comma

eosToken :: Parser Symbol
eosToken = satisfy $ isToken EOS

equalToken :: Parser Symbol
equalToken = satisfy $ isToken Equal

equalEqualToken :: Parser Symbol
equalEqualToken = satisfy $ isToken EqualEqual

greaterEqualToken :: Parser Symbol
greaterEqualToken = satisfy $ isToken GreaterEqual

greaterThanToken :: Parser Symbol
greaterThanToken = satisfy $ isToken GreaterThan

identifierToken :: Parser Symbol
identifierToken = satisfy $ isToken Identifier

inToken :: Parser Symbol
inToken = satisfy $ isToken In

lcurlyToken :: Parser Symbol
lcurlyToken = satisfy $ isToken LCurly

lessEqualToken :: Parser Symbol
lessEqualToken = satisfy $ isToken LessEqual

lessThanToken :: Parser Symbol
lessThanToken = satisfy $ isToken LessThan

literalIntToken :: Parser Symbol
literalIntToken = satisfy $ isToken LiteralInt

letToken :: Parser Symbol
letToken = satisfy $ isToken Let

letrecToken :: Parser Symbol
letrecToken = satisfy $ isToken Letrec

lparenToken :: Parser Symbol
lparenToken = satisfy $ isToken LParen

minusToken :: Parser Symbol
minusToken = satisfy $ isToken Minus

minusGreaterThanToken :: Parser Symbol
minusGreaterThanToken = satisfy $ isToken MinusGreaterThan

ofToken :: Parser Symbol
ofToken = satisfy $ isToken Of

packToken :: Parser Symbol
packToken = satisfy $ isToken Pack

periodToken :: Parser Symbol
periodToken = satisfy $ isToken Period

plusToken :: Parser Symbol
plusToken = satisfy $ isToken Plus

rcurlyToken :: Parser Symbol
rcurlyToken = satisfy $ isToken RCurly

rparenToken :: Parser Symbol
rparenToken = satisfy $ isToken RParen

semicolonToken :: Parser Symbol
semicolonToken = satisfy $ isToken Semicolon

slashToken :: Parser Symbol
slashToken = satisfy $ isToken Slash

starToken :: Parser Symbol
starToken = satisfy $ isToken Star

tildeEqualToken :: Parser Symbol
tildeEqualToken = satisfy $ isToken TildeEqual


isToken :: Token -> Symbol -> Bool
isToken t = (\s -> token s == t)

satisfy :: (Symbol -> Bool) -> Parser Symbol
satisfy f =
  let showTok :: Symbol -> String
      showTok t' = show t'

      posFromTok :: a -> b -> c -> P.SourcePos
      posFromTok _ _ _ = initialPos ""

      testTok :: Symbol -> Maybe Symbol
      testTok s = if f s then Just s else Nothing
   in P.tokenPrim showTok posFromTok testTok
