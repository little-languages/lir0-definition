module LIR0.Lexical
  ( Symbol
  , Token(..)
  , lexeme
  , scanner
  , token
  ) where

import qualified Data.List as List
import           Data.Char (isAlpha, isDigit, ord)

import           LIR0.Location

data Token
  = Case
  | In
  | Let
  | Letrec
  | Of
  | Pack
  | Identifier
  | LiteralInt
  | Ampersand
  | Backslash
  | Bar
  | Comma
  | Equal
  | EqualEqual
  | GreaterEqual
  | GreaterThan
  | LCurly
  | LessEqual
  | LessThan
  | LParen
  | Minus
  | MinusGreaterThan
  | Period
  | Plus
  | RCurly
  | RParen
  | Semicolon
  | Slash
  | Star
  | TildeEqual
  | WhiteSpace
  | SingleLineComment
  | MultiLineComment
  | EOS
  | LexicalError
  deriving (Eq, Show)

tokenPatterns :: [(String -> Maybe String, Token)]
tokenPatterns =
  [ (whitespace, WhiteSpace)
  , (singleLineComment, SingleLineComment)
  , (multiLineComment, MultiLineComment)
  , (literal "case", Case)
  , (literal "in", In)
  , (literal "letrec", Letrec)
  , (literal "let", Let)
  , (literal "of", Of)
  , (literal "Pack", Pack)
  , (identifier, Identifier)
  , (literalInt, LiteralInt)
  , (literal "&", Ampersand)
  , (literal "\\", Backslash)
  , (literal "|", Bar)
  , (literal ",", Comma)
  , (literal "==", EqualEqual)
  , (literal "=", Equal)
  , (literal ">=", GreaterEqual)
  , (literal ">", GreaterThan)
  , (literal "{", LCurly)
  , (literal "<=", LessEqual)
  , (literal "<", LessThan)
  , (literal "(", LParen)
  , (literal "->", MinusGreaterThan)
  , (literal "-", Minus)
  , (literal ".", Period)
  , (literal "+", Plus)
  , (literal "}", RCurly)
  , (literal ")", RParen)
  , (literal ";", Semicolon)
  , (literal "*", Star)
  , (literal "/", Slash)
  , (literal "~=", TildeEqual)
  ]

literal :: String -> String -> Maybe String
literal l s =
  if l `List.isPrefixOf` s
    then Just l
    else Nothing

identifier :: String -> Maybe String
identifier (h:r) | isAlpha h || h == '_' =
  Just $ h : List.takeWhile (\c -> isAlpha c || isDigit c || c == '_' ) r
identifier _ = Nothing

literalInt :: String -> Maybe String
literalInt (h:r) | isDigit h =
  Just $ h : List.takeWhile isDigit r
literalInt _ = Nothing

whitespace :: String -> Maybe String
whitespace t =
  let isWhitespace c = ord c < 33
   in case t of
        (h:r) | isWhitespace h -> Just $ h : takeWhile isWhitespace r
        _ -> Nothing

singleLineComment :: String -> Maybe String
singleLineComment ('/':'/':r) =
  Just $ '/':'/': List.takeWhile (\c -> ord c /= 10) r
singleLineComment _ = Nothing

multiLineComment :: String -> Maybe String
multiLineComment ('/':'*':r) =
  let isBody c =
        let ordC = ord c
         in ordC /= 42 && ordC /= 47

      tail (c:r) | isBody c = tail r >>= \r' -> Just $ c:r'
      tail ('/':'*':r) = tail r >>= \r' -> tail (drop (length r') r) >>= \r'' -> Just $ "/*" ++ r' ++ r''
      tail ('/':c:r) | isBody c = tail r >>= \r' -> Just $ '/':c:r'
      tail ('*':c:r) | isBody c = tail r >>= \r' -> Just $ '*':c:r'
      tail ('*':'/':_) = Just "*/"
      tail _ = Nothing
  in
    tail r >>= \r' -> Just $ '/':'*':r'
multiLineComment _ = Nothing

data Symbol
  = Symbol Token Location String
  deriving (Eq, Show)

instance Locationable Symbol where
  location (Symbol _ l _) = l

token :: Symbol -> Token
token (Symbol t _ _) = t

lexeme :: Symbol -> String
lexeme (Symbol _ _ l) = l

scanner :: String -> [Symbol]
scanner inp =
  let find :: String -> Maybe (String, Token)
      find inp' =
        find' inp' tokenPatterns

      find' :: String -> [(String -> Maybe String, Token)] -> Maybe (String, Token)
      find' _ [] = Nothing
      find' inp' (tp:tps) =
        case fst tp inp' of
          Nothing -> find' inp' tps
          Just s -> Just (s, snd tp)

      scanner' :: Int -> String -> [Symbol]
      scanner' idx "" =
        [Symbol EOS (mkPosition (Point idx) 0) ""]
      scanner' idx inp' =
        case find inp' of
          Nothing -> (Symbol LexicalError (mkPosition (Point idx) 0) (List.take 1 inp')) : (scanner' (idx + 1) (List.drop 1 inp'))
          Just (s, t) ->
            let length = List.length s
             in (Symbol t (mkPosition (Point idx) length)  s) : (scanner' (idx + length) (List.drop length inp'))
    in scanner' 0 inp