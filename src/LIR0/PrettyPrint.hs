module LIR0.PrettyPrint where

import           Data.ByteString.UTF8 (ByteString)
import qualified Data.HashMap.Strict  as Map
import qualified Data.Text            as Text
import qualified Data.Vector          as Vector
import qualified Data.Yaml            as Yaml
import qualified Data.Yaml.Pretty     as YamlPretty

valueToText :: Yaml.Value -> Text.Text
valueToText (Yaml.Object o) = Text.pack $ show o
valueToText (Yaml.Array a)  = Text.pack $ show a
valueToText (Yaml.String t) = t
valueToText (Yaml.Number s) = Text.pack $ show s
valueToText (Yaml.Bool b)   = Text.pack $ show b
valueToText Yaml.Null       = ""

class Yamlable y where
  toYaml :: y -> Yaml.Value

instance Yamlable o => Yamlable (Maybe o) where
  toYaml Nothing  = fromString "Nothing"
  toYaml (Just o) = fromMap [("Just", toYaml o)]

fromArray :: [Yaml.Value] -> Yaml.Value
fromArray = Yaml.Array . Vector.fromList

fromArray' :: Yamlable y => [y] -> Yaml.Value
fromArray' = fromArray . map toYaml

fromMap :: [(String, Yaml.Value)] -> Yaml.Value
fromMap = Yaml.Object . Map.fromList . map (\(k, v) -> (Text.pack k, v))

fromMap' :: String -> [(String, Yaml.Value)] -> Yaml.Value
fromMap' n s = fromMap [(n, fromMap s)]

fromString :: String -> Yaml.Value
fromString = Yaml.String . Text.pack

fromInt :: Int -> Yaml.Value
fromInt = Yaml.toJSON

pp :: Yaml.Value -> ByteString
pp = YamlPretty.encodePretty YamlPretty.defConfig
