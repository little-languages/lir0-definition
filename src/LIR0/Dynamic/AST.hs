module LIR0.Dynamic.AST where

import           Prelude          hiding ((<>))
import qualified Text.PrettyPrint as PP
import           Text.PrettyPrint ((<+>), (<>))


data Expr
  = EVar String
  | ENum Int
  | EConstr Int Int
  | EAp Expr Expr
  | ELet IsRec [(Name, Expr)] Expr
  | ECase Expr [Alter]
  | ELam [Name] Expr
  deriving (Eq, Show)

type Name =
  String

data IsRec
  = Recursive
  | NonRecursive
  deriving (Eq, Show)

bindersOf :: [(a, b)] -> [a]
bindersOf defns = [name | (name, _) <- defns]

rhsOf :: [(a, b)] -> [b]
rhsOf defns = [rhs | (_, rhs) <- defns]

type Alter =
  (Int, [Name], Expr)

isAtomicExpr :: Expr -> Bool
isAtomicExpr (EVar _) = True
isAtomicExpr (ENum _) = True
isAtomicExpr _ = False

type Program =
  [SCDefn]

type SCDefn =
  (Name, [Name], Expr)


ppProgram :: Program -> PP.Doc
ppProgram p = PP.vcat $ PP.punctuate (PP.text ";") $ map ppSCDefn p

ppSCDefn :: SCDefn -> PP.Doc
ppSCDefn (n, ns, e) =
  PP.text n <+> (PP.hsep $ map (\n -> PP.text n) ns) <+> PP.text "=" <+> ppExpr e

ppExpr :: Expr -> PP.Doc
ppExpr e =
  let ppExpr'' :: Int -> Int -> PP.Doc -> PP.Doc
      ppExpr'' fp tp d =
        if fp >= tp
          then PP.text "(" <> d <> PP.text ")"
          else d
      ppExpr' :: Int -> Expr -> PP.Doc
      ppExpr' _ (EVar s) =
        PP.text s
      ppExpr' _ (ENum n) =
        PP.int n
      ppExpr' _ (EConstr t a) =
        PP.text "Pack{" <> PP.int t <> PP.text ", " <> PP.int a <> PP.text "}"
      ppExpr' fp (EAp e1 e2) =
        ppExpr'' fp 6 $ ppExpr' 6 e1 <+> ppExpr' 6 e2
      ppExpr' fp (ELet Recursive decs e) =
        ppExpr'' fp 0 $
          PP.vcat
            [ PP.text "letrec" <+> PP.nest 0 (PP.vcat $ PP.punctuate (PP.text ";") $ map (\(n, e) -> PP.text n <+> PP.text "=" <+> ppExpr e) decs)
            , PP.text "in" <+> ppExpr e
            ]
      ppExpr' fp (ELet NonRecursive decs e) =
        ppExpr'' fp 0 $
          PP.vcat
            [ PP.text "let" <+> PP.nest 0 (PP.vcat $ PP.punctuate (PP.text ";") $ map (\(n, e) -> PP.text n <+> PP.text "=" <+> ppExpr e) decs)
            , PP.text "in" <+> ppExpr e
            ]
      ppExpr' fp (ECase e alts) =
        ppExpr'' fp 0 $
          PP.vcat
            [ PP.text "case" <+> ppExpr e <+> PP.text "of"
            , PP.nest 2 $ PP.vcat $ map (\(t, ns, e) ->
                PP.text "<" <> PP.int t <> PP.text ">" <+> (PP.hsep $ map PP.text ns) <+> PP.text "->" <+> ppExpr e) alts
            ]
      ppExpr' fp (ELam ns e) =
        ppExpr'' fp 0 $
          PP.text "\\" <> (PP.hsep $ map PP.text ns) <+> "." <+> ppExpr e
   in ppExpr' (-1) e