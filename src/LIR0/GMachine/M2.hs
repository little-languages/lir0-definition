module LIR0.GMachine.M2 where

import           Prelude            hiding ((<>))

import qualified Data.Map           as Map
import qualified Text.PrettyPrint   as PP
import           Text.PrettyPrint   ((<+>), (<>))

import qualified LIR0.GMachine.Heap as Heap

type GMState =
  (GMCode, GMStack, GMHeap, GMGlobals, GMStats)

getCode :: GMState -> GMCode
getCode (c, _, _, _, _) = c

putCode :: GMCode -> GMState -> GMState
putCode c' (_, s, h, g, st) =
  (c', s, h, g, st)

getStack :: GMState -> GMStack
getStack (_, s, _, _, _) = s

putStack :: GMStack -> GMState -> GMState
putStack s' (c, _, h, g, st) =
  (c, s', h, g, st)

getHeap :: GMState -> GMHeap
getHeap (_, _, h, _, _) = h

putHeap :: GMHeap -> GMState -> GMState
putHeap h' (c, s, _, g, st) =
  (c, s, h', g, st)

getGlobals :: GMState -> GMGlobals
getGlobals (_, _, _, g, _) = g

getStats :: GMState -> GMStats
getStats (_, _, _, _, st) = st

putStats :: GMStats -> GMState -> GMState
putStats st' (c, s, h, g, _) =
  (c, s, h, g, st')

type GMCode =
  [Instruction]

data Instruction
  = Unwind
  | PushGlobal String
  | PushInt Int
  | Push Int
  | MkAp
  | Update Int
  | Pop Int
  deriving (Eq, Show)

type GMHeap =
  Heap.Heap Node

data Node
  = NNum Int
  | NAp Heap.Address Heap.Address
  | NGlobal Int GMCode
  | NInd Heap.Address
  deriving (Eq, Show)

type GMStack =
  [Heap.Address]

type GMGlobals =
  Map.Map String Heap.Address

type GMStats =
  Int

statInitial :: GMStats
statInitial = 0

statIncSteps :: GMStats -> GMStats
statIncSteps s = s + 1

statGetSteps :: GMStats -> Int
statGetSteps s = s

eval :: GMState -> [GMState]
eval state =
  state : restStates
    where restStates | gmFinal state = []
                     | otherwise = eval nextState
          nextState = doAdmin (step state)

doAdmin :: GMState -> GMState
doAdmin s =
  putStats (statIncSteps $ getStats s) s

gmFinal :: GMState -> Bool
gmFinal s = getCode s == []

step :: GMState -> GMState
step s =
  dispatch i (putCode is s)
    where (i:is) = getCode s

dispatch :: Instruction -> GMState -> GMState
dispatch (PushGlobal v) = pushGlobal v
dispatch (PushInt n) = pushInt n
dispatch MkAp = mkAp
dispatch (Push n) = push n
dispatch (Update n) = update n
dispatch (Pop n) = pop n
dispatch Unwind = unwind

unwind :: GMState -> GMState
unwind s =
  newState (Heap.dereference a h')
    where (a:as) = getStack s
          h' = getHeap s
          newState (NNum _) = s
          newState (NAp a1 _) = putCode [Unwind] (putStack (a1:a:as) s)
          newState (NGlobal n c) | length as < n = error "Unwinding with too few argumemnts"
                                 | otherwise = putCode c s
          newState (NInd a') = putCode [Unwind] (putStack (a':as) s)

pushGlobal :: String -> GMState -> GMState
pushGlobal v s =
  putStack (a : getStack s) s
    where a = (getGlobals s) Map.! v

pushInt :: Int -> GMState -> GMState
pushInt n s =
  putHeap h' $ putStack (a : getStack s) s
    where (a, h') = Heap.alloc (NNum n) $ getHeap s

mkAp :: GMState -> GMState
mkAp s =
  putHeap h' $ putStack (a:as') s
    where (a, h') = Heap.alloc (NAp a1 a2) $ getHeap s
          (a1:a2:as') = getStack s

push :: Int -> GMState -> GMState
push n s =
  putStack (a:as) s
    where as = getStack s
          (NAp _ a) = Heap.dereference (as !! (n + 1)) $ getHeap s

slide :: Int -> GMState -> GMState
slide n s =
  putStack (a : drop n as) s
    where (a:as) = getStack s

update :: Int -> GMState -> GMState
update n s =
  putStack as $ putHeap h' $ s
    where (a:as) = getStack s
          h' = Heap.update (as !! n) (NInd a) (getHeap s)

pop :: Int -> GMState -> GMState
pop n s =
  putStack (drop n $ getStack s) s

showResults :: [GMState] -> PP.Doc
showResults states@(s:_) =
  PP.vcat
    [ PP.text "Supercombinator Definitions"
    , PP.nest 2 $ PP.vcat $ map (showSC s) (Map.assocs $ getGlobals s)
    , PP.text "State Transitions"
    , PP.nest 2 $ PP.vcat $ map showState states
    , PP.text "Steps Taken"
    , PP.nest 2 $ showStats $ last states
    ]

showSC :: GMState -> (String, Heap.Address) -> PP.Doc
showSC s (n, a) =
  PP.vcat
    [ PP.text n <> PP.text ":" <+> showAddr a
    , PP.nest 2 $ showInstructions code
    ]
    where (NGlobal _ code) = Heap.dereference a $ getHeap s

showInstructions :: GMCode -> PP.Doc
showInstructions is =
  PP.vcat $ map showInstruction is

showInstruction :: Instruction -> PP.Doc
showInstruction Unwind = PP.text "Unwind"
showInstruction (PushGlobal n) = PP.text "PushGlobal" <+> PP.text n
showInstruction (PushInt v) = PP.text "PushInt" <+> PP.int v
showInstruction (Push v) = PP.text "Push" <+> PP.int v
showInstruction MkAp = PP.text "MkAp"
showInstruction (Update v) = PP.text "Update" <+> PP.int v
showInstruction (Pop v) = PP.text "Pop" <+> PP.int v

showState :: GMState -> PP.Doc
showState s =
  PP.vcat
    [ PP.text "." <+>
        (PP.nest 0 $ PP.vcat
          [ showStack s
          , showInstructions $ getCode s
          ])
    ]

showStack :: GMState -> PP.Doc
showStack s =
  let showStackItem' = showStackItem s
      showStackItem'' i = PP.text "-" <+> showStackItem' i
   in PP.vcat $ map showStackItem'' (reverse $ getStack s)

showStackItem :: GMState -> Heap.Address -> PP.Doc
showStackItem s a =
  showAddr a <> PP.text ":" <+> showStackItemValue s a

showStackItemValue :: GMState -> Heap.Address -> PP.Doc
showStackItemValue s a =
  showNode s a (Heap.dereference a (getHeap s))

showNode :: GMState -> Heap.Address -> Node -> PP.Doc
showNode s a (NNum n) = PP.int n
showNode s a (NGlobal a' g) =
  let scs = Map.assocs $ Map.filter (\a'' -> a == a'') $ getGlobals s
      n = case scs of
            [] -> show a
            (n, _):_ -> n
   in PP.text "Global" <+> PP.text n
showNode s a (NAp a1 a2) =
  PP.text "Ap" <+> showAddr a1 <+> showAddr a2
showNode s a (NInd v) =
  PP.text "NInd" <+> showAddr v

showAddr :: Heap.Address -> PP.Doc
showAddr a =
  PP.text "#" <> PP.int a

showStats :: GMState -> PP.Doc
showStats s =
  PP.int $ getStats s