module LIR0.GMachine.M5.Compiler
  ( parse
  ) where

import qualified Data.Map           as Map

import qualified LIR0.Errors        as Errors
import qualified LIR0.Dynamic
import qualified LIR0.Dynamic.AST   as AST
import           LIR0.GMachine.M5
import qualified LIR0.GMachine.Heap as Heap

parse :: String -> Either Errors.Error GMState
parse src =
  fmap compile $ LIR0.Dynamic.parse src

compile :: AST.Program -> GMState
compile p =
  (initialCode, [], [], heap, globals, statInitial)
    where (heap, globals) = buildInitialHeap p

buildInitialHeap :: AST.Program -> (GMHeap, GMGlobals)
buildInitialHeap p =
  foldl allocateSc (Heap.initial, Map.empty) compiled
    where compiled =
            map compileSc (preludeDefs ++ p) ++ compiledPrimitives

type GMCompiledSC =
  (String, Int, GMCode)

allocateSc :: (GMHeap, GMGlobals) -> GMCompiledSC -> (GMHeap, GMGlobals)
allocateSc (h, g) (name, nargs, code) =
  (h', g')
    where (a, h') = Heap.alloc (NGlobal nargs code) h
          g' = Map.insert name a g

initialCode :: GMCode
initialCode = [PushGlobal "main", Eval]

compileSc :: AST.SCDefn -> GMCompiledSC
compileSc (name, args, body) =
  (name, length args, compileR body (Map.fromList $ zip args [0..]))

type GMCompiler =
  AST.Expr -> GMEnvironment -> GMCode

type GMEnvironment =
  Map.Map String Int

compileR :: GMCompiler
compileR e env =
  compileE e env ++ [Update d, Pop d, Unwind]
    where d = Map.size env

compileE :: GMCompiler
compileE (AST.ENum n) env = [PushInt n]
compileE (AST.ELet rec defs e) env
  | rec == AST.NonRecursive = compileLet compileE defs e env
  | rec == AST.Recursive = compileLetrec compileE defs e env
compileE (AST.EAp (AST.EAp (AST.EVar v) e1) e2) env
  | Map.member v builtInDyadic = compileE e2 env ++ compileE e1 (argOffset 1 env) ++ [builtInDyadic Map.! v]
compileE (AST.EAp (AST.EVar "negate") e) env =
  compileE e env ++ [Neg]
compileE (AST.EAp (AST.EAp (AST.EAp (AST.EVar "if") e0) e1) e2) env =
  compileE e0 env ++ [Cond (compileE e1 env) (compileE e2 env)]
compileE a env = compileC a env ++ [Eval]

compileC :: GMCompiler
compileC (AST.EVar v) env | Map.member v env = [Push $ env Map.! v]
                          | otherwise = [PushGlobal v]
compileC (AST.ENum n) env = [PushInt n]
compileC (AST.EAp e1 e2) env =
  compileC e2 env ++ compileC e1 (argOffset 1 env) ++ [MkAp]
compileC (AST.ELet rec defs e) env
  | rec == AST.NonRecursive = compileLet compileC defs e env
  | rec == AST.Recursive = compileLetrec compileC defs e env

compileLet :: GMCompiler -> [(AST.Name, AST.Expr)] -> GMCompiler
compileLet compiler defs expr env =
  compileLet' defs env ++ compiler expr env' ++ [Slide $ length defs]
    where env' = compileArgs defs env

compileLet' :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMCode
compileLet' [] env = []
compileLet' ((n, e): defs) env = compileC e env ++ compileLet' defs (argOffset 1 env)

compileLetrec :: GMCompiler -> [(AST.Name, AST.Expr)] -> GMCompiler
compileLetrec compiler defs expr env =
  [Alloc $ length defs] ++ compileLetrec' defs env' ++ compiler expr env' ++ [Slide $ length defs]
    where env' = compileArgs defs env

compileLetrec' :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMCode
compileLetrec' [] env = []
compileLetrec' ((n, e): defs) env = compileC e env ++ [Update $ length defs] ++ compileLetrec' defs env

compileArgs :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMEnvironment
compileArgs defs env =
  Map.union defs' (argOffset n env)
    where defs' = Map.fromAscList $ zip nms [n-1, n-2 .. 0]
          nms = map fst defs
          n = length defs

argOffset :: Int -> GMEnvironment -> GMEnvironment
argOffset n env =
  Map.map (+ n) env

builtInDyadic :: Map.Map AST.Name Instruction
builtInDyadic =
  Map.fromList
    [ ("+", Add)
    , ("-", Sub)
    , ("*", Mul)
    , ("/", Div)
    , ("==", Eq)
    , ("~=", Ne)
    , ("<", Lt)
    , ("<=", Le)
    , (">", Gt)
    , (">=", Ge)
    ]

compiledPrimitives :: [GMCompiledSC]
compiledPrimitives =
  [ ("+", 2, [Push 1, Eval, Push 1, Eval, Add, Update 2, Pop 2, Unwind])
  , ("-", 2, [Push 1, Eval, Push 1, Eval, Sub, Update 2, Pop 2, Unwind])
  , ("*", 2, [Push 1, Eval, Push 1, Eval, Mul, Update 2, Pop 2, Unwind])
  , ("/", 2, [Push 1, Eval, Push 1, Eval, Div, Update 2, Pop 2, Unwind])
  , ("negate", 1, [Push 0, Eval, Neg, Update 1, Pop 1, Unwind])
  , ("==", 2, [Push 1, Eval, Push 1, Eval, Eq, Update 2, Pop 2, Unwind])
  , ("~=", 2, [Push 1, Eval, Push 1, Eval, Ne, Update 2, Pop 2, Unwind])
  , ("<", 2, [Push 1, Eval, Push 1, Eval, Lt, Update 2, Pop 2, Unwind])
  , ("<=", 2, [Push 1, Eval, Push 1, Eval, Le, Update 2, Pop 2, Unwind])
  , (">", 2, [Push 1, Eval, Push 1, Eval, Gt, Update 2, Pop 2, Unwind])
  , (">=", 2, [Push 1, Eval, Push 1, Eval, Ge, Update 2, Pop 2, Unwind])
  , ("if", 3, [Push 0, Eval, Cond [Push 1] [Push 2], Update 3, Pop 3, Unwind])
  ]

preludeDefs :: AST.Program
preludeDefs =
  [ ("I", ["x"], AST.EVar "x")
  , ("K", ["x", "y"], AST.EVar "x")
  , ("K1", ["x", "y"], AST.EVar "y")
  , ("S", ["f", "g", "x"], AST.EAp (AST.EAp (AST.EVar "f") (AST.EVar "x")) (AST.EAp (AST.EVar "g") (AST.EVar "x")))
  , ("compose", ["f", "g", "x"], AST.EAp (AST.EVar "f") (AST.EAp (AST.EVar "g") (AST.EVar "x")))
  , ("twice", ["f"], AST.EAp (AST.EAp (AST.EVar "compose") (AST.EVar "f")) (AST.EVar "f"))
  ]