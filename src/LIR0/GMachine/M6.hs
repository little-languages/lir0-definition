module LIR0.GMachine.M6 where

import           Prelude            hiding ((<>), print)

import qualified Data.List          as List
import qualified Data.Map           as Map
import qualified Data.Maybe         as Maybe
import qualified Text.PrettyPrint   as PP
import           Text.PrettyPrint   ((<+>), (<>))

import qualified LIR0.GMachine.Heap as Heap

type GMState =
  (GMOutput, GMCode, GMStack, GMDump, GMHeap, GMGlobals, GMStats)

getOutput :: GMState -> GMOutput
getOutput (o, _, _, _, _, _, _) = o

putOutput :: GMOutput -> GMState -> GMState
putOutput o' (_, c, s, d, h, g, st) =
  (o', c, s, d, h, g, st)

getCode :: GMState -> GMCode
getCode (_, c, _, _, _, _, _) = c

putCode :: GMCode -> GMState -> GMState
putCode c' (o, _, s, d, h, g, st) =
  (o, c', s, d, h, g, st)

getStack :: GMState -> GMStack
getStack (_, _, s, _, _, _, _) = s

putStack :: GMStack -> GMState -> GMState
putStack s' (o, c, _, d, h, g, st) =
  (o, c, s', d, h, g, st)

getDump :: GMState -> GMDump
getDump (_, _, _, d, _, _, _) = d

putDump :: GMDump -> GMState -> GMState
putDump d' (o, c, s, _, h, g, st) =
  (o, c, s, d', h, g, st)

getHeap :: GMState -> GMHeap
getHeap (_, _, _, _, h, _, _) = h

putHeap :: GMHeap -> GMState -> GMState
putHeap h' (o, c, s, d, _, g, st) =
  (o, c, s, d, h', g, st)

getGlobals :: GMState -> GMGlobals
getGlobals (_, _, _, _, _, g, _) = g

getStats :: GMState -> GMStats
getStats (_, _, _, _, _, _, st) = st

putStats :: GMStats -> GMState -> GMState
putStats st' (o, c, s, d, h, g, _) =
  (o, c, s, d, h, g, st')

type GMOutput =
  String

type GMCode =
  [Instruction]

data Instruction
  = Unwind
  | PushGlobal String
  | PushInt Int
  | Push Int
  | MkAp
  | Update Int
  | Pop Int
  | Slide Int
  | Alloc Int
  | Eval
  | Add | Sub | Mul | Div | Neg
  | Eq | Ne | Lt | Le | Gt | Ge
  | Cond GMCode GMCode
  | Pack Int Int
  | Casejump [(Int, GMCode)]
  | Split Int
  | Print
  deriving (Eq, Show)

type GMDump =
  [GMDumpItem]

type GMDumpItem =
  (GMCode, GMStack)

type GMHeap =
  Heap.Heap Node

data Node
  = NNum Int
  | NAp Heap.Address Heap.Address
  | NGlobal Int GMCode
  | NInd Heap.Address
  | NConstr Int [Heap.Address]
  deriving (Eq, Show)

type GMStack =
  [Heap.Address]

type GMGlobals =
  Map.Map String Heap.Address

type GMStats =
  Int

statInitial :: GMStats
statInitial = 0

statIncSteps :: GMStats -> GMStats
statIncSteps s = s + 1

statGetSteps :: GMStats -> Int
statGetSteps s = s

eval :: GMState -> [GMState]
eval state =
  state : restStates
    where restStates | gmFinal state = []
                     | otherwise = eval nextState
          nextState = doAdmin (step state)

doAdmin :: GMState -> GMState
doAdmin s =
  putStats (statIncSteps $ getStats s) s

gmFinal :: GMState -> Bool
gmFinal s = getCode s == []

step :: GMState -> GMState
step s =
  dispatch i (putCode is s)
    where (i:is) = getCode s

dispatch :: Instruction -> GMState -> GMState
dispatch (PushGlobal v) = pushGlobal v
dispatch (PushInt n) = pushInt n
dispatch MkAp = mkAp
dispatch (Push n) = push n
dispatch (Update n) = update n
dispatch (Pop n) = pop n
dispatch Unwind = unwind
dispatch (Slide n) = slide n
dispatch (Alloc n) = alloc n
dispatch Eval = eval'
dispatch Add = opAdd
dispatch Sub = opSub
dispatch Mul = opMul
dispatch Div = opDiv
dispatch Neg = opNeg
dispatch Eq = opEq
dispatch Ne = opNe
dispatch Lt = opLt
dispatch Le = opLe
dispatch Gt = opGt
dispatch Ge = opGe
dispatch (Cond i1 i2) = opCond i1 i2
dispatch (Pack t n) = pack t n
dispatch (Casejump alts) = casejump alts
dispatch (Split n) = split n
dispatch Print = print

unwind :: GMState -> GMState
unwind s =
  newState (Heap.dereference a h')
    where (a:as) = getStack s
          d = getDump s
          h' = getHeap s

          newState (NNum _)
            | d == [] = s
            | otherwise = let ((i', s'):ds') = d
                           in putCode i' $ putStack (a:s') $ putDump ds' s
          newState (NConstr _ _)
            | d == [] = s
            | otherwise = let ((i', s'):ds') = d
                           in putCode i' $ putStack (a:s') $ putDump ds' s
          newState (NAp a1 _) = putCode [Unwind] (putStack (a1:a:as) s)
          newState (NGlobal n c) | length as < n && length d > 0 =
                                     let ((i', s'):ds') = d
                                      in putCode i' $ putStack ((last $ getStack s) : s') $ putDump ds' s
                                 | length as < n = error "Unwinding with too few argumemnts"
                                 | otherwise = putCode c $ putStack (rearrange n (getStack s)) s
          newState (NInd a') = putCode [Unwind] (putStack (a':as) s)

          rearrange :: Int -> GMStack -> GMStack
          rearrange n as =
            let as' = map getArg $ map (\a -> Heap.dereference a h') (tail as)
             in take n as' ++ drop n as

getArg :: Node -> Heap.Address
getArg (NAp _ e) = e

pushGlobal :: String -> GMState -> GMState
pushGlobal v s =
  putStack (a : getStack s) s
    where a = (getGlobals s) Map.! v

pushInt :: Int -> GMState -> GMState
pushInt n s =
  putHeap h' $ putStack (a : getStack s) s
    where (a, h') = Heap.alloc (NNum n) $ getHeap s

mkAp :: GMState -> GMState
mkAp s =
  putHeap h' $ putStack (a:as') s
    where (a, h') = Heap.alloc (NAp a1 a2) $ getHeap s
          (a1:a2:as') = getStack s

push :: Int -> GMState -> GMState
push n s =
  putStack (a:as) s
    where as = getStack s
          a = as !! n

update :: Int -> GMState -> GMState
update n s =
  putStack as $ putHeap h' $ s
    where (a:as) = getStack s
          h' = Heap.update (as !! n) (NInd a) (getHeap s)

pop :: Int -> GMState -> GMState
pop n s =
  putStack (drop n $ getStack s) s

slide :: Int -> GMState -> GMState
slide n s =
  putStack (a : drop n as) s
    where (a:as) = getStack s

alloc :: Int -> GMState -> GMState
alloc n s =
  let allocNode :: Int -> GMHeap -> ([Heap.Address], GMHeap)
      allocNode 0 h = ([], h)
      allocNode n h =
        let (a, h') = Heap.alloc (NInd Heap.nullAddress) h
            (as, h'') = allocNode (n-1) h'
         in (a:as, h'')
      (as, h) = allocNode n (getHeap s)
   in putHeap h $ putStack (as ++ (getStack s)) s

eval' :: GMState -> GMState
eval' s =
  putCode [Unwind] $ putStack (take 1 $ getStack s) $ putDump ((getCode s, drop 1 $ getStack s):getDump s) s

opAdd :: GMState -> GMState
opAdd = arithmetic2 (+)

opSub :: GMState -> GMState
opSub = arithmetic2 (-)

opMul :: GMState -> GMState
opMul = arithmetic2 (*)

opDiv :: GMState -> GMState
opDiv = arithmetic2 quot

opNeg :: GMState -> GMState
opNeg = arithmetic1 ((-) 0)

opEq :: GMState -> GMState
opEq = comparison (==)

opNe :: GMState -> GMState
opNe = comparison (/=)

opLt :: GMState -> GMState
opLt = comparison (<)

opLe :: GMState -> GMState
opLe = comparison (<=)

opGt :: GMState -> GMState
opGt = comparison (>)

opGe :: GMState -> GMState
opGe = comparison (>=)

opCond :: GMCode -> GMCode -> GMState -> GMState
opCond i1 i2 s =
  putStack as $ putCode (i' ++ (getCode s)) s
    where (a:as) = getStack s
          c = unboxInteger a s
          i' = if c == 1 then i1 else i2

pack :: Int -> Int -> GMState -> GMState
pack t n s =
  putStack (a:drop n st) $ putHeap h' s
    where st = getStack s
          (a, h') = Heap.alloc (NConstr t (take n st)) $ getHeap s

casejump :: [(Int, GMCode)] -> GMState -> GMState
casejump alts s =
  putCode (i' ++ getCode s) s
    where (a:_) = getStack s
          (NConstr t ss) = Heap.dereference a $ getHeap s
          (t', i') = Maybe.fromJust $ List.find (\(t', _) -> t' == t) alts

split :: Int -> GMState -> GMState
split n s =
  putStack (ss ++ as) s
    where (a:as) = getStack s
          (NConstr t ss) = Heap.dereference a $ getHeap s

print :: GMState -> GMState
print s =
    case n of
      NNum v -> putStack as $ appendOutput (show v) s
      NConstr t vs -> putStack (vs ++ as) $ putCode ((concat $ replicate (length vs) [Eval, Print]) ++ getCode s) s
    where (a:as) = getStack s
          n = Heap.dereference a $ getHeap s
          appendOutput txt s =
            if getOutput s == ""
              then putOutput txt s
              else putOutput (getOutput s ++ "\n" ++ txt) s

boxInteger :: Int -> GMState -> GMState
boxInteger n s =
  putStack (a:getStack s) $ putHeap h' s
    where (a, h') = Heap.alloc (NNum n) $ getHeap s

unboxInteger :: Heap.Address -> GMState -> Int
unboxInteger a s =
  unbox $ Heap.dereference a $ getHeap s
    where unbox (NNum n) = n
          unbox t = error $ show t

boxBoolean :: Bool -> GMState -> GMState
boxBoolean False = boxInteger 0
boxBoolean True = boxInteger 1

unboxBoolean :: Heap.Address -> GMState -> Bool
unboxBoolean a state
  | unboxInteger a state == 0 = False
  | otherwise = True

primitive1 :: (a -> GMState -> GMState) -> (Heap.Address -> GMState -> b) -> (b -> a) -> GMState -> GMState
primitive1 box unbox op state =
  box (op (unbox a state)) (putStack as state)
    where (a:as) = getStack state

primitive2 :: (a -> GMState -> GMState) -> (Heap.Address -> GMState -> b) -> (b -> b -> a) -> GMState -> GMState
primitive2 box unbox op state =
  box (op (unbox a1 state) (unbox a2 state)) (putStack as state)
    where (a1:a2:as) = getStack state

arithmetic1 :: (Int -> Int) -> GMState -> GMState
arithmetic1 = primitive1 boxInteger unboxInteger

arithmetic2 :: (Int -> Int -> Int) -> GMState -> GMState
arithmetic2 = primitive2 boxInteger unboxInteger

comparison :: (Int -> Int -> Bool) -> GMState -> GMState
comparison = primitive2 boxBoolean unboxInteger

showResults :: [GMState] -> PP.Doc
showResults states@(s:_) =
  PP.vcat
    [ PP.text "Supercombinator Definitions"
    , PP.nest 2 $ PP.vcat $ map (showSC s) (Map.assocs $ getGlobals s)
    , PP.text "State Transitions"
    , PP.nest 2 $ PP.vcat $ map showState states
    , PP.text "Steps Taken"
    , PP.nest 2 $ showStats $ last states
    ]

showSC :: GMState -> (String, Heap.Address) -> PP.Doc
showSC s (n, a) =
  PP.vcat
    [ PP.text n <> PP.text ":" <+> showAddr a
    , PP.nest 2 $ showInstructions code
    ]
    where (NGlobal _ code) = Heap.dereference a $ getHeap s

showInstructions :: GMCode -> PP.Doc
showInstructions is =
  PP.vcat $ map showInstruction is

showInstruction :: Instruction -> PP.Doc
showInstruction Unwind = PP.text "Unwind"
showInstruction (PushGlobal n) = PP.text "PushGlobal" <+> PP.text n
showInstruction (PushInt v) = PP.text "PushInt" <+> PP.int v
showInstruction (Push v) = PP.text "Push" <+> PP.int v
showInstruction MkAp = PP.text "MkAp"
showInstruction (Update v) = PP.text "Update" <+> PP.int v
showInstruction (Pop v) = PP.text "Pop" <+> PP.int v
showInstruction (Slide v) = PP.text "Slide" <+> PP.int v
showInstruction (Alloc v) = PP.text "Alloc" <+> PP.int v
showInstruction Eval = PP.text "Eval"
showInstruction Add = PP.text "Add"
showInstruction Sub = PP.text "Sub"
showInstruction Mul = PP.text "Mul"
showInstruction Div = PP.text "Div"
showInstruction Neg = PP.text "Neg"
showInstruction Eq = PP.text "Eq"
showInstruction Ne = PP.text "Ne"
showInstruction Lt = PP.text "Lt"
showInstruction Le = PP.text "Le"
showInstruction Gt = PP.text "Gt"
showInstruction Ge = PP.text "Ge"
showInstruction (Cond i1 i2) = PP.text "Cond" <+> showInstructions i1 <+> showInstructions i2
showInstruction (Pack t a) = PP.text "Pack" <+> PP.int t <+> PP.int a
showInstruction (Casejump alts) = PP.text "Casejump"
showInstruction (Split n) = PP.text "Split" <+> PP.int n
showInstruction Print = PP.text "Print"


showState :: GMState -> PP.Doc
showState s =
        (PP.nest 0 $ PP.vcat
          [ PP.text "-------------------------------------" <+> PP.int (getStats s)
          , PP.text "\"" <> PP.text (getOutput s) <> PP.text "\""
          , showStack s
          , showDump s
          , showInstructions $ getCode s
          ])

showStack :: GMState -> PP.Doc
showStack s =
  let showStackItem' = showStackItem s
      showStackItem'' i = PP.text "-" <+> showStackItem' i
   in PP.vcat $ map showStackItem'' (reverse $ getStack s)

showStackItem :: GMState -> Heap.Address -> PP.Doc
showStackItem s a =
  showAddr a <> PP.text ":" <+> showStackItemValue s a

showStackItemValue :: GMState -> Heap.Address -> PP.Doc
showStackItemValue s a =
  showNode s a (Heap.dereference a (getHeap s))

showNode :: GMState -> Heap.Address -> Node -> PP.Doc
showNode s a (NNum n) = PP.int n
showNode s a (NGlobal a' g) =
  let scs = Map.assocs $ Map.filter (\a'' -> a == a'') $ getGlobals s
      n = case scs of
            [] -> show a
            (n, _):_ -> n
   in PP.text "Global" <+> PP.text n
showNode s a (NAp a1 a2) =
  PP.text "Ap" <+> showAddr a1 <+> showAddr a2
showNode s a (NInd v) =
  PP.text "NInd" <+> showAddr v
showNode s _ (NConstr t as) =
  let h = getHeap s
   in PP.text "Constr" <+> PP.int t <+> PP.text "[" <> PP.hcat (PP.punctuate (PP.text ", ") (map (\a' -> showNode s a' (Heap.dereference a' h)) as)) <> PP.text "]"

showAddr :: Heap.Address -> PP.Doc
showAddr a =
  PP.text "#" <> PP.int a

showStats :: GMState -> PP.Doc
showStats s =
  PP.int $ getStats s

showDump :: GMState -> PP.Doc
showDump s =
  PP.vcat $ map (showDumpItem s) $ getDump s

showDumpItem :: GMState -> GMDumpItem -> PP.Doc
showDumpItem s (c, st) =
  PP.text ">>" <+> (PP.nest 0 $ PP.vcat [ showSummaryInstructions c
                                        , showSummaryStack s st
                                        ]) <+> PP.text "<<"
showSummaryInstructions :: GMCode -> PP.Doc
showSummaryInstructions is =
  PP.vcat $ map showInstruction $ take 3 is

showSummaryStack :: GMState -> GMStack -> PP.Doc
showSummaryStack s st =
  let showStackItem' = showStackItem s
      showStackItem'' i = PP.text "-" <+> showStackItem' i
   in PP.vcat $ map showStackItem'' (reverse st)
