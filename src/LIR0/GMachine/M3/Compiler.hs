module LIR0.GMachine.M3.Compiler
  ( parse
  ) where

import qualified Data.Map           as Map

import qualified LIR0.Errors        as Errors
import qualified LIR0.Dynamic
import qualified LIR0.Dynamic.AST   as AST
import           LIR0.GMachine.M3
import qualified LIR0.GMachine.Heap as Heap

parse :: String -> Either Errors.Error GMState
parse src =
  fmap compile $ LIR0.Dynamic.parse src

compile :: AST.Program -> GMState
compile p =
  (initialCode, [], heap, globals, statInitial)
    where (heap, globals) = buildInitialHeap p

buildInitialHeap :: AST.Program -> (GMHeap, GMGlobals)
buildInitialHeap p =
  foldl allocateSc (Heap.initial, Map.empty) compiled
    where compiled =
            map compileSc (preludeDefs ++ p) ++ compiledPrimitives

type GMCompiledSC =
  (String, Int, GMCode)

allocateSc :: (GMHeap, GMGlobals) -> GMCompiledSC -> (GMHeap, GMGlobals)
allocateSc (h, g) (name, nargs, code) =
  (h', g')
    where (a, h') = Heap.alloc (NGlobal nargs code) h
          g' = Map.insert name a g

initialCode :: GMCode
initialCode = [PushGlobal "main", Unwind]

compileSc :: AST.SCDefn -> GMCompiledSC
compileSc (name, args, body) =
  (name, length args, compileR body (Map.fromList $ zip args [0..]))

type GMCompiler =
  AST.Expr -> GMEnvironment -> GMCode

type GMEnvironment =
  Map.Map String Int

compileR :: GMCompiler
compileR e env =
  compileC e env ++ [Update d, Pop d, Unwind]
    where d = Map.size env

compileC :: GMCompiler
compileC (AST.EVar v) env | Map.member v env = [Push $ env Map.! v]
                          | otherwise = [PushGlobal v]
compileC (AST.ENum n) env = [PushInt n]
compileC (AST.EAp e1 e2) env =
  compileC e2 env ++ compileC e1 (argOffset 1 env) ++ [MkAp]
compileC (AST.ELet rec defs e) env
  | rec == AST.NonRecursive = compileLet defs e env
  | rec == AST.Recursive = compileLetrec defs e env

compileLet :: [(AST.Name, AST.Expr)] -> GMCompiler
compileLet defs expr env =
  compileLet' defs env ++ compileC expr env' ++ [Slide $ length defs]
    where env' = compileArgs defs env

compileLet' :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMCode
compileLet' [] env = []
compileLet' ((n, e): defs) env = compileC e env ++ compileLet' defs (argOffset 1 env)

compileLetrec :: [(AST.Name, AST.Expr)] -> GMCompiler
compileLetrec defs expr env =
  [Alloc $ length defs] ++ compileLetrec' defs env' ++ compileC expr env' ++ [Slide $ length defs]
    where env' = compileArgs defs env

compileLetrec' :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMCode
compileLetrec' [] env = []
compileLetrec' ((n, e): defs) env = compileC e env ++ [Update $ length defs] ++ compileLet' defs env

compileArgs :: [(AST.Name, AST.Expr)] -> GMEnvironment -> GMEnvironment
compileArgs defs env =
  Map.union defs' (argOffset n env)
    where defs' = Map.fromAscList $ zip nms [n-1, n-2 .. 0]
          nms = map fst defs
          n = length defs

argOffset :: Int -> GMEnvironment -> GMEnvironment
argOffset n env =
  Map.map (+ n) env

compiledPrimitives :: [GMCompiledSC]
compiledPrimitives = []


preludeDefs :: AST.Program
preludeDefs =
  [ ("I", ["x"], AST.EVar "x")
  , ("K", ["x", "y"], AST.EVar "x")
  , ("K1", ["x", "y"], AST.EVar "y")
  , ("S", ["f", "g", "x"], AST.EAp (AST.EAp (AST.EVar "f") (AST.EVar "x")) (AST.EAp (AST.EVar "g") (AST.EVar "x")))
  , ("compose", ["f", "g", "x"], AST.EAp (AST.EVar "f") (AST.EAp (AST.EVar "g") (AST.EVar "x")))
  , ("twice", ["f"], AST.EAp (AST.EAp (AST.EVar "compose") (AST.EVar "f")) (AST.EVar "f"))
  ]