module LIR0.GMachine.Heap
  ( Heap
  , Address
  , alloc
  , dereference
  , free
  , isNullAddress
  , initial
  , nullAddress
  , update
  ) where

import qualified Data.Map as Map

type Heap a
  = (Int, [Int], Map.Map Int a)

type Address =
  Int

initial :: Heap a
initial = (0, [1..], Map.empty)

alloc :: a -> Heap a -> (Address, Heap a)
alloc n (size, next:free, cts) = (next, (size + 1, free, Map.insert next n cts))

dereference :: Address -> Heap a -> a
dereference a (_, _, cts) = cts Map.! a

update :: Address -> a -> Heap a -> Heap a
update a n (size, free, cts) = (size, free, Map.insert a n cts )

free :: Heap a -> Address -> Heap a
free (size, free, cts) a =
  if Map.member a cts
    then (size - 1, a : free, Map.delete a cts)
    else (size, free, cts)

nullAddress :: Address
nullAddress = 0

isNullAddress :: Address -> Bool
isNullAddress a = a == nullAddress