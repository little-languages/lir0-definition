module LIR0.Dynamic
  ( parse
  ) where

import qualified LIR0.Errors      as Errors
import qualified LIR0.Static
import           LIR0.Static.AST
import qualified LIR0.Dynamic.AST as AST

parse :: String -> Either Errors.Error AST.Program
parse src =
  fmap translateP $ LIR0.Static.parse src

translateP :: Program -> AST.Program
translateP (Program scs) =
  map translateSC scs

translateSC :: SCDefn -> AST.SCDefn
translateSC (SCDefn _ n ns e) =
  (translateName n, map translateName ns, translateE e)

translateName :: Name -> AST.Name
translateName (Name _ s) =
  s

translateE :: Expr -> AST.Expr
translateE (EVar _ s) = AST.EVar s
translateE (ENum (ConstantInt _ s)) = AST.ENum ((read s) :: Int)
translateE (EConstr _ (ConstantInt _ t) (ConstantInt _ a)) = AST.EConstr ((read t) :: Int) ((read a) :: Int)
translateE (EParen _ e) = translateE e
translateE (EAp e1 e2) = AST.EAp (translateE e1) (translateE e2)
translateE (ELet _ r a e) = AST.ELet (translateR r) (map (\(n, e) -> (translateName n, translateE e)) a) (translateE e)
translateE (ECase _ e als) = AST.ECase (translateE e) (map translateA als)
translateE (ELam _ ns e) = AST.ELam (map translateName ns) (translateE e)

translateR :: IsRec -> AST.IsRec
translateR Recursive = AST.Recursive
translateR NonRecursive = AST.NonRecursive

translateA :: Alter -> AST.Alter
translateA (Alter _ (ConstantInt _ s) ns e) =
  ((read s) :: Int, map translateName ns, translateE e)