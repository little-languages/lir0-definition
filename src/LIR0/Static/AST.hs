module LIR0.Static.AST where

import LIR0.Location
import LIR0.PrettyPrint

data Expr
  = EVar Location String
  | ENum ConstantInt
  | EConstr Location ConstantInt ConstantInt
  | EParen Location Expr
  | EAp Expr Expr
  | ELet Location IsRec [(Name, Expr)] Expr
  | ECase Location Expr [Alter]
  | ELam Location [Name] Expr
  deriving (Eq, Show)

instance Locationable Expr where
  location (EVar l _) = l
  location (ENum (ConstantInt l _)) = l
  location (EConstr l _ _) = l
  location (EParen l _) = l
  location (EAp e1 e2) = (location e1) `combine` (location e2)
  location (ELet l _ _ _) = l
  location (ECase l _ _) = l
  location (ELam l _ _) = l

data Name
  = Name Location String
  deriving (Eq, Show)

data ConstantInt
  = ConstantInt Location String
  deriving (Eq, Show)

data IsRec
  = Recursive
  | NonRecursive
  deriving (Eq, Show)

data Alter
  = Alter Location ConstantInt [Name] Expr
  deriving (Eq, Show)

data Program
  = Program [SCDefn]
  deriving (Eq, Show)

data SCDefn
  = SCDefn Location Name [Name] Expr
  deriving (Eq, Show)

instance Locationable Name where
  location (Name l _) = l

instance Locationable Alter where
  location (Alter l _ _ _) = l

instance Yamlable Name where
  toYaml (Name l n) = fromMap [("location", toYaml l), ("value", fromString n)]

instance Yamlable ConstantInt where
  toYaml (ConstantInt l v) = fromMap [("location", toYaml l), ("value", fromString v)]

instance Yamlable IsRec where
  toYaml Recursive = fromString "Recursive"
  toYaml NonRecursive = fromString "NonRecursive"

instance Yamlable Expr where
  toYaml (EVar l v) = fromMap' "EVar" [("location", toYaml l), ("value", fromString v)]
  toYaml (ENum v) = fromMap [("ENum", toYaml v)]
  toYaml (EConstr l t a) = fromMap' "EConstr" [("location", toYaml l), ("tag", toYaml t), ("arity", toYaml a)]
  toYaml (EParen l e) = fromMap' "EParen" [("location", toYaml l), ("e", toYaml e)]
  toYaml (EAp e1 e2) = fromMap' "EAp" [("e1", toYaml e1), ("e2", toYaml e2)]
  toYaml (ELet l isRec ps e) = fromMap' "ELet" [("location", toYaml l), ("isRec", toYaml isRec), ("ps", fromArray (map (\(n, e) -> fromMap [("n", toYaml n), ("e", toYaml e)]) ps)), ("e", toYaml e)]
  toYaml (ECase l e as) = fromMap' "ECase" [("location", toYaml l), ("e", toYaml e), ("as", fromArray $ map toYaml as)]
  toYaml (ELam l ns e) = fromMap' "ELam" [("location", toYaml l), ("ns", fromArray $ map toYaml ns), ("e", toYaml e)]

instance Yamlable Alter where
  toYaml (Alter l c ns e) = fromMap [("location", toYaml l), ("c", toYaml c), ("ns", fromArray $ map toYaml ns), ("e", toYaml e)]

instance Yamlable SCDefn where
  toYaml (SCDefn l n ns e) = fromMap [("location", toYaml l), ("n", toYaml n), ("ns", fromArray $ map toYaml ns), ("e", toYaml e)]

instance Yamlable Program where
  toYaml (Program scs) = fromArray $ map toYaml scs
